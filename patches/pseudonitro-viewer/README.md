## DisTok CutTheCord: Pseudo Nitro Viewer Patch

This patch allows viewing emojis sent with pseudoNitro.

pseudoNitro is a way of sending emojis from all guilds to all guilds, even without Nitro. Both receivers need psuedoNitro to view these.
- The old format is inserting a `&` between `<` and `:` (such as `<&:thinkEyes:406800811700781076>`).
- The new format is inserting a zwsp between `<` and `:` (such as `<​:thinkEyes:406800811700781076>`).
- This patch supports receiving both.

TL;DR:

![](https://lasagna.cat/i/nw0kfk2r.png)

#### Available and tested on:
- 24
- 28-alpha2
- 28.1
- 29-alpha1
- 30.0
- 30.1
- 31-alpha1
- 31-alpha2
- 32-alpha2
- 32.0
- 33.1
- 34.0
- 34.2
- 34.3
- 35.0-alpha1
- 36.3
- 36.5
- 38.0
- 38.1
- 40.04
- 41.02
- 41.05
- 41.06
- 41.10
- 41.11
- 42.0
- 42.1
- 42.3
- 44-alpha2
- 44-alpha4
- 44.5
- 44.6
- 45.2
- 46.0
- 46.3
- 48.0
- 48.1
- 48.2
- 49.1
- 49.2
- 49.8
- 49.10
- 49.12
- 49.13
- 52.3
- 53.0

